/***********************************************************************
/*  Name: exp_insert_dbo_pattern_param.sql
/*  Description: ODI DBO Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 3/4/2014  Vinh Mai            Initial Version
/**********************************************************************/
-- Insert new interface
delete from exp_odi_interface where interface_id = 'I_DBO_MERCHHIER_RMS_ALC';
insert into exp_odi_interface (interface_id, description, source_app, target_app, pattern_type) values ('I_DBO_MERCHHIER_RMS_ALC', 'Testing for DBO pattern','RMS','ALC','DBO');


insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBO_MERCHHIER_RMS_ALC','DBO','RESTART_NAME','alcmerchhierdb');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBO_MERCHHIER_RMS_ALC','DBO','RESTART_NAME_ARCHIVE','alcmerchhierarcdb');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBO_MERCHHIER_RMS_ALC','DBO','RETENTION_HISTORY_DAYS','180');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBO_MERCHHIER_RMS_ALC','DBO','STAGING_TABLE','EXP_STG_DBOTEST');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBO_MERCHHIER_RMS_ALC','DBO','ARCHIVE_TABLE','EXP_HIST_DBOTEST');

--Populate the RESTART_CONTROL table with the new job name/info.
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('alcmerchhierdb', 'DBO Sample processing','NONE',1,'N','T',1000);
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('alcmerchhierarcdb', 'DBO Sample archive','NONE',1,'N','T',1000);

insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('alcmerchhierdb',1,'alcmerchhierdb','ready for start');
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('alcmerchhierarcdb',1,'alcmerchhierarcdb','ready for start');
commit;

-- Create the example staging table
create table EXP_STG_DBOTEST as select * from deps;
create table EXP_HIST_DBOTEST as (select t.*, sysdate archive_date from EXP_STG_DBOTEST t where 1=0);
create public synonym EXP_STG_DBOTEST for expr_integration.EXP_STG_DBOTEST;
create public synonym EXP_HIST_DBOTEST for expr_integration.EXP_HIST_DBOTEST;

