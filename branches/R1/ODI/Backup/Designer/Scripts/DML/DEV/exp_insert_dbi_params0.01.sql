/***********************************************************************
/*  Name: Sample Table
/*  Description: Sample DBI table for ODI framework
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 
/**********************************************************************/
set define off
-- RUN IN RMS Schema
--Populate the RESTART_PROGRAM_STATUS and set the program_status to be �ready for start�.
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('paptermsdb',1,'paptermsdb','ready for start');
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('paptermsdb',2,'paptermsdb','ready for start');
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('paptermsdb',3,'paptermsdb','ready for start');


-- RUN in EXPR_INTEGRATION schema.
alter table EXP_ODI_INTERFACE_PARAM modify param_value varchar2(255);

-- Insert new pattern parameters
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','RESTART_NAME_STAGING','RMS Restart Program Name for Staging Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','RESTART_NAME','RMS Restart Program Name.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','JNDI_URL','JNDI URL','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','RESTART_NAME_ARCHIVE','RMS Restart Program Name for Archive Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','RETENTION_HISTORY_DAYS','Days to keep history.','Y','N');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','STAGING_TABLE_HEAD','Name of the staging header.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','STAGING_TABLE_DETAIL','Name of the staging table (i.e detail).','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','ARCHIVE_TABLE_HEAD','Name of the archive table (i.e. header).','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','ARCHIVE_TABLE_DETAIL','Name of the archive table (i.e. detail).','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','ARCHIVE_HEADER_FLAG','A flag to determine whether the staging header will be archived or not.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBI','ARCHIVE_DETAIL_FLAG','A flag to determine whether the staging detail table will be archived or not.','Y','V');


-- Insert new interface
insert into exp_odi_interface (interface_id, description, source_app, target_app, pattern_type) values ('I_DBI_TERMS_PAP_RMS', 'Testing for DBI pattern','PAP','RMS','DBI');


-- clear out existing value on interfae param table
delete from EXP_ODI_INTERFACE_PARAM where interface_id = 'I_DBI_TERMS_PAP_RMS';

-- Insert interface parameters
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','RESTART_NAME_STAGING','paptermsstgdb');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','RESTART_NAME','paptermsdb');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','JNDI_URL','t3://cmhldesoaapp01.expdev.local:8001?d=\\\\cmhfiler2\\exp_it\\app-specific-shares\\odiRMS\\AP_PAY_TERM_FULLSYNC_EFF.dtd&s=CMHDSOA&JMS_DESTINATION=jms/etEXTPayTermFromHRIS&nobu=false&ro=true');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','RESTART_NAME_ARCHIVE','paptermsarcdb');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','RETENTION_HISTORY_DAYS','180');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','STAGING_TABLE_HEAD','EXP_PAYTERM_HEAD_STG');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','STAGING_TABLE_DETAIL','EXP_PAYTERM_DTL_STG');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','ARCHIVE_TABLE_HEAD','EXP_PAYTERM_HEAD_HIST');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','ARCHIVE_TABLE_DETAIL','EXP_PAYTERM_DTL_HIST');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','ARCHIVE_HEADER_FLAG','Y');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID , PATTERN_TYPE, PARAM_NAME, PARAM_VALUE)  values ('I_DBI_TERMS_PAP_RMS','DBI','ARCHIVE_DETAIL_FLAG','Y');


--Populate the RESTART_CONTROL table with the new job name/info.
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('paptermsdb', 'DBI Sample processing','NONE',3,'N','T',1000);
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('paptermsarcdb', 'DBI Sample archive','NONE',1,'N','T',1000);
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('paptermsstgdb', 'DBI Sample staging','NONE',1,'N','T',1000);


insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('paptermsarcdb',1,'paptermsarcdb','ready for start');
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('paptermsstgdb',1,'paptermsstgdb','ready for start');

 --EXP_PAYTERMDESC_REC
  drop table EXPR_INTEGRATION.EXP_PAYTERM_HEAD_STG;
 CREATE TABLE "EXPR_INTEGRATION"."EXP_PAYTERM_HEAD_STG"
  (
    "TERMS"        VARCHAR2(15 BYTE),
    "TERMS_CODE"   VARCHAR2(50 BYTE),
    "TERMS_DESC"   VARCHAR2(240 BYTE),
    "DUE_DAYS"     NUMBER(3,0),
    "ENABLED_FLAG" VARCHAR2(1 BYTE),
    "START_DATE_ACTIVE" DATE,
    "END_DATE_ACTIVE" DATE,
    "DISCDAYS" NUMBER(3,0),
    "PERCENT"  NUMBER(12,4),
    "RANK"     NUMBER(10,0)
    ,THREAD_NUM NUMBER 
    ,PROCESSED_IND VARCHAR2(1) default 'N' -- 'Y'es or 'N'o
    ,LAST_UPDATE_ID VARCHAR2(80)
    ,LAST_UPDATE_DATETIME DATE 	
	,CREATE_ID    VARCHAR2(30)
	,CREATE_DATETIME DATE
  );
 

 
 
CREATE OR REPLACE TRIGGER exp_odi_dbi_testhead_t1
      BEFORE INSERT ON EXP_PAYTERM_HEAD_STG
      FOR EACH ROW
BEGIN
:NEW.CREATE_DATETIME := SYSDATE;
:NEW.CREATE_ID := USER;
END;
 
CREATE OR REPLACE TRIGGER exp_odi_dbi_testhead_t2
      BEFORE INSERT OR UPDATE ON EXP_PAYTERM_HEAD_STG
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;
	
  create public synonym EXP_PAYTERM_HEAD_STG for expr_integration.EXP_PAYTERM_HEAD_STG;
  create public synonym EXP_PAYTERM_DTL_STG for expr_integration.EXP_PAYTERM_DTL_STG;


  
 --EXP_PAYTERMDTL_REC
 drop table EXPR_INTEGRATION.EXP_PAYTERM_DTL_STG;
 CREATE TABLE EXPR_INTEGRATION.EXP_PAYTERM_DTL_STG
  (
     TERMS        VARCHAR2(15 BYTE)
    ,DUE_DAYS       NUMBER(3,0)
    ,DUE_MAX_AMOUNT NUMBER(12,4)
    ,DUE_DOM        NUMBER(2,0)
    ,DISCDAYS       NUMBER(3,0)
    ,PERCENT        NUMBER(12,4)
    ,DISC_DOM       NUMBER(2,0)
    ,DISC_MM_FWD    NUMBER(3,0)
    ,FIXED_DATE DATE
    ,ENABLED_FLAG VARCHAR2(1 BYTE)
    ,START_DATE_ACTIVE DATE
    ,END_DATE_ACTIVE DATE
    ,TERMS_SEQ  NUMBER(10,0)
    ,DUE_MM_FWD NUMBER(3,0)
    ,CUTOFF_DAY NUMBER(2,0)
  );
  
  
 create table EXPR_INTEGRATION.EXP_PAYTERM_HEAD_HIST as (select t.*, sysdate archive_date from EXP_PAYTERM_HEAD_STG t where 1=0);
 create table EXPR_INTEGRATION.EXP_PAYTERM_DTL_HIST as (select t.*, sysdate archive_date from EXP_PAYTERM_DTL_STG t where 1=0);
   create public synonym EXP_PAYTERM_HEAD_HIST for expr_integration.EXP_PAYTERM_HEAD_HIST;
  create public synonym EXP_PAYTERM_DTL_HIST for expr_integration.EXP_PAYTERM_DTL_HIST;


  
 -- Insert into the table
delete from EXP_PAYTERM_HEAD_STG where terms = 'test01';
delete from EXP_PAYTERM_DTL_STG where terms = 'test01';
--
delete from EXP_PAYTERM_HEAD_STG where terms = 'test02';
delete from EXP_PAYTERM_DTL_STG where terms = 'test02';
-- end tables
delete from terms_head where terms = 'test01';
delete from terms_head where terms = 'test02';
delete from terms_detail where terms = 'test01';
delete from terms_detail where terms = 'test02';
  
 insert into EXP_PAYTERM_HEAD_STG (terms, terms_code, terms_desc,rank, enabled_flag, thread_num) values ('test01','test01','test term',1,'Y',1);
insert into EXP_PAYTERM_DTL_STG (terms, due_days, due_max_amount, due_dom, discdays, percent, disc_dom, disc_mm_fwd, fixed_date, enabled_flag, start_date_active, end_date_active, terms_seq, due_mm_fwd, cutoff_day) 
values ('test01',998, 1, null,null,1,null,null,null,'Y',null, null,1,null,1);
insert into EXP_PAYTERM_DTL_STG (terms, due_days, due_max_amount, due_dom, discdays, percent, disc_dom, disc_mm_fwd, fixed_date, enabled_flag, start_date_active, end_date_active, terms_seq, due_mm_fwd, cutoff_day) 
values ('test01',999, 1, null,null,1,null,null,null,'Y',null, null,2,null,1);

 -- Insert into the table

 insert into EXP_PAYTERM_HEAD_STG (terms, terms_code, terms_desc,rank, enabled_flag, thread_num) values ('test02','test02','test term',2,'Y',2);
insert into EXP_PAYTERM_DTL_STG (terms, due_days, due_max_amount, due_dom, discdays, percent, disc_dom, disc_mm_fwd, fixed_date, enabled_flag, start_date_active, end_date_active, terms_seq, due_mm_fwd, cutoff_day) 
values ('test02',998, 1, null,null,1,null,null,null,'Y',null, null,1,null,1);
insert into EXP_PAYTERM_DTL_STG (terms, due_days, due_max_amount, due_dom, discdays, percent, disc_dom, disc_mm_fwd, fixed_date, enabled_flag, start_date_active, end_date_active, terms_seq, due_mm_fwd, cutoff_day) 
values ('test02',999, 1, null,null,1,null,null,null,'Y',null, null,2,null,1);

