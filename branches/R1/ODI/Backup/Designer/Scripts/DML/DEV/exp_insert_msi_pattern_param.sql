/***********************************************************************
/*  Name: exp_insert_msi_params.sql
/*  Description: ODI MSI Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 3/5/2014  Vinh Mai            Initial Version
/**********************************************************************/
-- Insert new pattern parameters
delete from EXP_ODI_PATTERN_PARAM where pattern_type = 'MSI';

insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','ARCHIVE_DIR','Archive directory location','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','RESTART_NAME_ARCHIVE','RMS Restart Program Name for Archive Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','RESTART_NAME_STAGING','RMS Restart Program Name for Staging Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','RESTART_NAME','RMS Restart Program Name.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','SOURCE_DIR','The name of the inbound file.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','MULTI_FILENAME_MASK','The file mask used to retrieve a list of files from the source directory','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','STAGING_TABLE','Name of the staging table','Y','V');
--insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','DSN_NAME','The data source name that is configured in the ODBC settings.  This is a pointer to a filename.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('MSI','RESOURCE_NAME','The name of the data area in the Excel file.','Y','V');

commit;
