/***********************************************************************
/*  Name: exp_insert_ffi_params.sql
/*  Description: ODI FFI Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 
/**********************************************************************/
insert into exp_odi_pattern_param (pattern_type, param_name, description, required_flag, param_type)
 values ('FFO', 'MAX_SEQUENCE_ID', 'Max sequence ID value, default to 9999999', 'N','N');