-- EXP_ODI_APPLICATION
delete from exp_odi_interface_param;
delete from exp_odi_pattern_param where pattern_type = 'FFO';
delete from exp_odi_application;

insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ADS','ADS - Express Card');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('POS','NCR POS AS US');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PSC','NCR POS AS CA');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('AJB','AJB');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ASH','Al Shaya');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('AMS','NCR AMS (CA)');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ATG','ATG');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ATT','Attributes Database');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('AVR','Avery');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('BIZ','EAI');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('BRI','Brierley - Loyaltyware');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('CRT','Central Returns');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('CKP','Checkpoint - Loss Prevention');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DWH','Data Warehouse - Chain');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DWF','Data Warehouse - Franchise');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DHL','DHL - Warehouse Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('EAS','EASi - Real-Estate Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('EDR','Enterprise Data Repository');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E02','Microsoft Excel - Finance');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E01','Microsoft Excel - Store Annualized Volume');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E03','Microsoft Excel - Production / Sourcing');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E04','Microsoft Excel - Inventory Control');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('FRW','Farrow - Commercial Invoice Generation');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('F01','Franchise-AXO');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('F02','Franchise-Fastco');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ALC','JDA Allocation');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('JSS','JDA Size Scaling');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('LPM','LPMS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MF','Mainframe');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MLS','Manual Labor Scheduling');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MMF','MGT LPAR Mainframe');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MGT','MGT Store Master');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('NPD','NPD Fashionworld');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('HRI','PeopleSoft HRIS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PAP','PeopleSoft AP');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PGL','PeopleSoft GL');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PIM','Product Information Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PKM','PkMS - Warehouse Management System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PML','Performance Management Labor System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PDB','PO Database - Vendor Follow-Up');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PAS','PO Franchise Database - Vendor Follow-Up');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PPS','Production Planning System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RCN','ReConNet - Cash and Charge Settlement');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('REF','Reflexis - Store task management system ');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RMS','Oracle Retail Merchandising System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RA','Oracle Retail Analytics');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RIM','Oracle Retail Invoice Matching');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RPM','Oracle Retail Price Management System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('SIM','Oracle Retail Store Inventory Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RIB','Oracle Retail Integration Bus');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RSA','Oracle Retail Sales Audit');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RGS','RGIS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ROC','Rockport');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('EDI','EDI');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('STR','Store Applications');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('TRI','Trilogy - Ecom Sales Fulfillment');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('VNT','VendorNet');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('WIS','WIS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('WKB','Workbrain - Time Tracking');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('XBR','Loss Prevention');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('SVS','Express Gift Card');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PDM','Product Data Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('JEP','Enterprise Planning');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DWE','Data Warehouse - Employees');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('GAF','Google Affiliate Network');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('IP','Island Pacific');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('OIM','Oracle Identity Manager');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PAR','PeopleSoft AR');


--EXP_ODI_PATTERN
insert into EXP_ODI_PATTERN (PATTERN_TYPE ,DESCRIPTION) values ('FFO','Flat File Out');
insert into EXP_ODI_PATTERN (PATTERN_TYPE ,DESCRIPTION) values ('FFI','Flat File In');
insert into EXP_ODI_PATTERN (PATTERN_TYPE ,DESCRIPTION) values ('DBO','Database Out');
insert into EXP_ODI_PATTERN (PATTERN_TYPE ,DESCRIPTION) values ('DBI','Database In');
insert into EXP_ODI_PATTERN (PATTERN_TYPE ,DESCRIPTION) values ('MSI','Microsoft In');


-- EXP_ODI_PATTERN_PARAM 

insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','STD_INTERFACE_ID','Express Standard Interface ID','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','DETAIL_REC_SIZE','Detail Record Size','N','N');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','INTERFACE_VERSION','Interface version','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','INTERFACE_STANDARD','Header specific interface version','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SOURCE_DIR','Source directory location','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SOURCE_FILE','Source Default Flat file name','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','TARGET_DIR','Target directory location','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','TARGET_FILE','Target Default Flat file name','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','ARCHIVE_DIR','Archive directory location','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','ARCHIVE_FILE','Archive Default Flat file name','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SFTP_HOSTNAME','Hostname of the target server','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SFTP_USER_ID','SFTP User ID of the target server','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SFTP_ENCODED_PASSWORD','SFTP encoded password of the target server','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SEQUENCE_ID','Express Sequence ID','N','N');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','ENVIRONMENT','PROD,TEST,TRN,DEV','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','SOURCE_NODE','Name of the node sending the file.','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','TARGET_NODE','Name of the node receiving the file','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','BATCH_CONTROL_XX','Express Batch control number','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','CONTROL_TOTAL','Express Batch control number','N','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE, PARAM_NAME, DESCRIPTION, REQUIRED_FLAG,PARAM_TYPE) 
values('FFO','RESTART_NAME','RMS Restart Program Name','N','V');

--EXP_ODI_INTERFACE_PARAM 

--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','STD_INTERFACE_ID','INTEASI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','DETAIL_REC_SIZE','0895');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','INTERFACE_VERSION','1');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','INTERFACE_STANDARD','5');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SOURCE_DIR', '/tmp');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SOURCE_FILE','INTEASI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','TARGET_DIR', '/tmp');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','TARGET_FILE','244_STORES_RMS_ALC.dat');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','ARCHIVE_DIR', '/tmp');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','ARCHIVE_FILE','244_STORES_RMS_ALC.dat');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SFTP_HOSTNAME','cmhldocnvodb01');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SFTP_USER_ID','vmai');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SFTP_ENCODED_PASSWORD','d,ypDOJ6AIoPmT.hjD2Ziw2uI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SEQUENCE_ID','1000');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','ENVIRONMENT','PROD');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SOURCE_NODE','CMHWPEASI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','TARGET_NODE','CMHPJDADB');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','BATCH_CONTROL_XX','00');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','CONTROL_TOTAL','0');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','RESTART_NAME','alcstores');

--EXP_ODI_INTERFACE
--insert into EXP_ODI_INTERFACE (INTERFACE_ID ,DESCRIPTION,SOURCE_APP,TARGET_APP,PATTERN_TYPE) values ('244_STORES_RMS_ALC','Stores from RMS to Allocations','RMS','ALC','FFO');

--RESTART_CONTROL
--insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr)
--values ('alcstores', 'alcstores','NONE',1,'N','T',1000);

--RESTART_PROGRAM_STATUS
-- INSERT FOR 10 threads
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',1,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',2,'alcstores','ready for start');
--INSERT INTO RESTART_PROGRAM_STATUS (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS) VALUES ('alcstores',3,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',4,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',5,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',6,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',7,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',8,'alcstores','ready for start');
--INSERT INTO RESTART_PROGRAM_STATUS (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS) VALUES ('alcstores',9,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',10,'alcstores','ready for start');

commit;
 
  
