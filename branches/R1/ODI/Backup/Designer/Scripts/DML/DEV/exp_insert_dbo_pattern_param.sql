/***********************************************************************
/*  Name: exp_insert_dbo_pattern_param.sql
/*  Description: ODI DBO Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 3/4/2014  Vinh Mai            Initial Version
/**********************************************************************/
-- Insert new pattern parameters
delete from EXP_ODI_PATTERN_PARAM where pattern_type = 'DBO';
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBO','RESTART_NAME','RMS Restart Program Name.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBO','RESTART_NAME_ARCHIVE','RMS Restart Program Name for Archive Process','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBO','RETENTION_HISTORY_DAYS','Days to keep history.','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBO','STAGING_TABLE','Name of the staging table','Y','V');
insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE , PARAM_NAME, Description, Required_flag, Param_type)  values ('DBO','ARCHIVE_TABLE','Name of the archive table.','Y','V');

commit;
