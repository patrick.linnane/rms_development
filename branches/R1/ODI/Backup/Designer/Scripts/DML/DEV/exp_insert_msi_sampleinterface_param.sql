/***********************************************************************
/*  Name: exp_insert_msi_pattern_param.sql
/*  Description: ODI MSI Pattern parameters
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 3/5/2014  Vinh Mai            Initial Version
/**********************************************************************/
-- Insert new interface
delete from exp_odi_interface where interface_id = 'I_MSI_STORES_E02_RMS';
insert into exp_odi_interface (interface_id, description, source_app, target_app, pattern_type) values ('I_MSI_STORES_E02_RMS', 'Testing for MSI pattern','RMS','ALC','MSI');

insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','ARCHIVE_DIR','/data/odi_stage/msi_stores_e02_rms/archive');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','RESTART_NAME_ARCHIVE','e02storesmsiarc');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','RESTART_NAME_STAGING','e02storesmsistg');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','RESTART_NAME','e02storesmsi');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','SOURCE_DIR','/data/odi_stage/msi_stores_e02_rms/inbound');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','MULTI_FILENAME_MASK','*AnVol_Q*.xls');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','STAGING_TABLE','EXP_STG_MSITEST');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE  , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','DSN_NAME','e02storesmsi');
insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID, PATTERN_TYPE  , PARAM_NAME, PARAM_VALUE)  values ('I_MSI_STORES_E02_RMS','MSI','RESOURCE_NAME','AnnVol');




-- Run in RMS13
--Populate the RESTART_CONTROL table with the new job name/info.
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('e02storesmsistg', 'MSI Sample stage loading','NONE',1,'N','T',1000);
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('e02storesmsi', 'MSI Sample processing','NONE',1,'N','T',1000);
insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr) values ('e02storesmsiarc', 'MSI Sample archive','NONE',1,'N','T',1000);

insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('e02storesmsistg',1,'e02storesmsistg','ready for start');
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('e02storesmsi',1,'e02storesmsi','ready for start');
insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status)
values ('e02storesmsiarc',1,'e02storesmsiarc','ready for start');
commit;

-- Create a staging table
CREATE TABLE EXPR_INTEGRATION.EXP_STG_MSITEST
(
    STORE NUMBER(10,0)
    ,VOL   NUMBER(10,0)
    ,THREAD_NUM NUMBER 
    ,PROCESSED_IND VARCHAR2(1) default 'N' -- 'Y'es or 'N'o
    ,LAST_UPDATE_ID VARCHAR2(80)
    ,LAST_UPDATE_DATETIME DATE 	
);
-- Create the trigger
set define off
CREATE OR REPLACE TRIGGER EXPR_INTEGRATION.EXP_STG_MSITEST_TRG1 BEFORE
  INSERT OR
  UPDATE ON EXP_STG_MSITEST FOR EACH ROW BEGIN :NEW.LAST_UPDATE_DATETIME := SYSDATE;
  :NEW.LAST_UPDATE_ID := USER;
END;
/

-- Create the example staging table
--create table EXP_STG_MSITEST as select * from deps;
create public synonym EXP_STG_MSITEST for expr_integration.EXP_STG_MSITEST;

CREATE TABLE EXPR_INTEGRATION.EXP_MSI_TEST_TGT
  (
    STORE NUMBER(10,0),
    VOL   NUMBER(10,0)
  );

CREATE PUBLIC SYNONYM EXP_MSI_TEST_TGT FOR expr_integration.EXP_MSI_TEST_TGT;

