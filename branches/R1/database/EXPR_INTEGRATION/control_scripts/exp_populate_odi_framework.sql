/*  Name: Exp_ODI_Framework
/*  Description: Poopulate the ODI framework required table, run in EXPR_INTEGRATION schema
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 2014-09-03 Sajan Khadka			Populate all the tables required for ODI framework
/* 
/**********************************************************************/

delete from exp_odi_interface_param;
delete from exp_odi_pattern_param;
delete from exp_odi_pattern;
delete from exp_odi_application;

insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ADS','ADS - Express Card');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('POS','NCR POS AS US');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PSC','NCR POS AS CA');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('AJB','AJB');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ASH','Al Shaya');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('AMS','NCR AMS (CA)');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ATG','ATG');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ATT','Attributes Database');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('AVR','Avery');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('BIZ','EAI');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('BRI','Brierley - Loyaltyware');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('CRT','Central Returns');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('CKP','Checkpoint - Loss Prevention');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DWH','Data Warehouse - Chain');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DWF','Data Warehouse - Franchise');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DHL','DHL - Warehouse Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('EAS','EASi - Real-Estate Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('EDR','Enterprise Data Repository');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E02','Microsoft Excel - Finance');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E01','Microsoft Excel - Store Annualized Volume');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E03','Microsoft Excel - Production / Sourcing');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('E04','Microsoft Excel - Inventory Control');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('FRW','Farrow - Commercial Invoice Generation');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('F01','Franchise-AXO');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('F02','Franchise-Fastco');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ALC','JDA Allocation');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('JSS','JDA Size Scaling');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('LPM','LPMS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MF','Mainframe');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MLS','Manual Labor Scheduling');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MMF','MGT LPAR Mainframe');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('MGT','MGT Store Master');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('NPD','NPD Fashionworld');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('HRI','PeopleSoft HRIS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PAP','PeopleSoft AP');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PGL','PeopleSoft GL');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PIM','Product Information Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PKM','PkMS - Warehouse Management System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PML','Performance Management Labor System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PDB','PO Database - Vendor Follow-Up');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PAS','PO Franchise Database - Vendor Follow-Up');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PPS','Production Planning System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RCN','ReConNet - Cash and Charge Settlement');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('REF','Reflexis - Store task management system ');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RMS','Oracle Retail Merchandising System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RA','Oracle Retail Analytics');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RIM','Oracle Retail Invoice Matching');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RPM','Oracle Retail Price Management System');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('SIM','Oracle Retail Store Inventory Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RIB','Oracle Retail Integration Bus');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RSA','Oracle Retail Sales Audit');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('RGS','RGIS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('ROC','Rockport');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('EDI','EDI');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('STR','Store Applications');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('TRI','Trilogy - Ecom Sales Fulfillment');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('VNT','VendorNet');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('WIS','WIS');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('WKB','Workbrain - Time Tracking');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('XBR','Loss Prevention');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('SVS','Express Gift Card');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PDM','Product Data Management');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('JEP','Enterprise Planning');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('DWE','Data Warehouse - Employees');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('GAF','Google Affiliate Network');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('IP','Island Pacific');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('OIM','Oracle Identity Manager');
insert into exp_odi_application (APPLICATION_CODE,APPLICATION_NAME) values ('PAR','PeopleSoft AR');


--EXP_ODI_PATTERN

insert into EXP_ODI_PATTERN (PATTERN_TYPE ,DESCRIPTION) values ('DBO','Database Out');

-- EXP_ODI_PATTERN_PARAM 

Insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE,PARAM_NAME,DESCRIPTION,REQUIRED_FLAG,PARAM_TYPE) values ('DBO','LOADING_PACKAGE_DELTA','Name of the delta load package','Y','V');
Insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE,PARAM_NAME,DESCRIPTION,REQUIRED_FLAG,PARAM_TYPE) values ('DBO','LOADING_PACKAGE_FULL','Name of the full load package','Y','V');
Insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE,PARAM_NAME,DESCRIPTION,REQUIRED_FLAG,PARAM_TYPE) values ('DBO','RESTART_NAME_FULL','RMS Restart Program Name for Full load','Y','V');
Insert into EXP_ODI_PATTERN_PARAM (PATTERN_TYPE,PARAM_NAME,DESCRIPTION,REQUIRED_FLAG,PARAM_TYPE) values ('DBO','RESTART_NAME','RMS Restart Program Name for Delta load','Y','V');


--EXP_ODI_INTERFACE_PARAM 

--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','STD_INTERFACE_ID','INTEASI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','DETAIL_REC_SIZE','0895');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','INTERFACE_VERSION','1');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','INTERFACE_STANDARD','5');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SOURCE_DIR', '/tmp');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SOURCE_FILE','INTEASI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','TARGET_DIR', '/tmp');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','TARGET_FILE','244_STORES_RMS_ALC.dat');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','ARCHIVE_DIR', '/tmp');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','ARCHIVE_FILE','244_STORES_RMS_ALC.dat');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SFTP_HOSTNAME','cmhldocnvodb01');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SFTP_USER_ID','vmai');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SFTP_ENCODED_PASSWORD','d,ypDOJ6AIoPmT.hjD2Ziw2uI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SEQUENCE_ID','1000');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','ENVIRONMENT','PROD');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','SOURCE_NODE','CMHWPEASI');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','TARGET_NODE','CMHPJDADB');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','BATCH_CONTROL_XX','00');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','CONTROL_TOTAL','0');
--insert into EXP_ODI_INTERFACE_PARAM (INTERFACE_ID ,PATTERN_TYPE, PARAM_NAME, PARAM_VALUE) values ('244_STORES_RMS_ALC','FFO','RESTART_NAME','alcstores');

--EXP_ODI_INTERFACE
--insert into EXP_ODI_INTERFACE (INTERFACE_ID ,DESCRIPTION,SOURCE_APP,TARGET_APP,PATTERN_TYPE) values ('244_STORES_RMS_ALC','Stores from RMS to Allocations','RMS','ALC','FFO');

--RESTART_CONTROL
--insert into RESTART_CONTROL (program_name, program_desc, driver_name, num_threads, update_allowed, process_flag, commit_max_ctr)
--values ('alcstores', 'alcstores','NONE',1,'N','T',1000);

--RESTART_PROGRAM_STATUS
-- INSERT FOR 10 threads
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',1,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',2,'alcstores','ready for start');
--INSERT INTO RESTART_PROGRAM_STATUS (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS) VALUES ('alcstores',3,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',4,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',5,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',6,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',7,'alcstores','ready for start');
--insert into RESTART_PROGRAM_STATUS (restart_name, thread_val, program_name, program_status) values ('alcstores',8,'alcstores','ready for start');
--INSERT INTO RESTART_PROGRAM_STATUS (RESTART_NAME, THREAD_VAL, PROGRAM_NAME, PROGRAM_STATUS) VALUES ('alcstores',9,'a