/*  Name: Exp_ODI_Framework
/*  Description: Setup the ODI framework required triggers, run in EXPR_INTEGRATION schema
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 2014-09-03 Sajan Khadka			Trigger to log all the changes in EXP_ODI_PUB_INFO tables.
 
/**********************************************************************/

CREATE OR REPLACE TRIGGER EXP_ODI_PUB_INFO_TO_LOG_TRG1
	 AFTER UPDATE OR DELETE ON EXP_ODI_PUB_INFO
      FOR EACH ROW
	  DECLARE 
	begin
insert into EXP_ODI_PUB_INFO_LOG
		(
			INTERFACE_ID,
			TABLE_ID,
			LAST_INT_TIME
						
		)
		select	
			:old.INTERFACE_ID,
			:old.TABLE_ID,
			:old.LAST_INT_TIME
			
		from	dual;
	end;