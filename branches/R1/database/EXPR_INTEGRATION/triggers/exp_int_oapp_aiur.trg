/*  Name: Exp_ODI_Framework
/*  Description: Setup the ODI framework required triggers, run in EXPR_INTEGRATION schema
/* Modification History: 
/* Date      Name                Modification Description
/* --------  -----------------   -------------------------------------
/* 2014-09-03 Sajan Khadka			Trigger to update LAST_UPDATE_DATETIME and LAST_UPDATE_ID after each insert or update in EXP_ODI_APPLICATION table
/* 
/**********************************************************************/





CREATE OR REPLACE TRIGGER exp_odi_app_trg1
      BEFORE INSERT OR UPDATE ON EXP_ODI_APPLICATION
      FOR EACH ROW
BEGIN
:NEW.LAST_UPDATE_DATETIME := SYSDATE;
:NEW.LAST_UPDATE_ID := USER;
END;