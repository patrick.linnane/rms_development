/*******************************************************************************************************
/*           Name: exp_gap_356_po_batch_ins_restart_ctrl.sql
/*    Object Type: Insert Script
/*    Description: Insert records into RESTART_CONTROL for the custom batch exp_bacth_ordcostcompupd.ksh
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 03-JUN-2014  Baranidharan G          Initial Version
/*******************************************************************************************************/


WHENEVER SQLERROR EXIT 1

PROMPT Delete the existing records from RESTART_CONTROL table

DELETE FROM RESTART_CONTROL
      WHERE PROGRAM_NAME ='exp_batch_ordcostcompupd';

PROMPT Inserting records into RESTART_CONTROL table

INSERT INTO RESTART_CONTROL (PROGRAM_NAME,
                             PROGRAM_DESC,
                             DRIVER_NAME,
                             NUM_THREADS,
                             UPDATE_ALLOWED,
                             PROCESS_FLAG,
                             COMMIT_MAX_CTR,
                             LOCK_WAIT_TIME,
                             RETRY_MAX_CTR) 
                     values ('exp_batch_ordcostcompupd',
                             'Express ordcostcomp batch' ,
                             'ORDER',
                             1,
                             'Y',
                             'T',
                             1,
                             10,
                             3);

