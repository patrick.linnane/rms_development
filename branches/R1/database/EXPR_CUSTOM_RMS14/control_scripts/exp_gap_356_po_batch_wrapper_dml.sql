/****************************************************************************************/
/*           Name: exp_gap_356_po_batch_wrapper_dml.sql
/*    Object Type: DML wrapper Script
/*    Description: This script calls the insert script 
/*                 exp_gap_356_po_batch_ins_restart_ctrl.sql
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 18-SEP-2014  Baranidharan              Initial Version
/**************************************************************************************/

spool exp_gap_356_po_batch_wrapper_dml.log

@exp_gap_356_po_batch_ins_restart_ctrl.sql

spool off
