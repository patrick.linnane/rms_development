/****************************************************************************************/
/*           Name: exp_gap_356_po_batch_wrapper_ddl.sql
/*    Object Type: DDL wrapper Script
/*    Description: This script calls the table scripts exp_cost_comp_temp.tab and 
/*                 exp_ord_comp_assess.tab in EXPR_CUSTOM_MODS schema.
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 18-SEP-2014  Baranidharan              Initial Version
/**************************************************************************************/

spool exp_gap_356_po_batch_wrapper_ddl.log

@exp_gap_create_exp_cost_comp_temp.tab
@exp_gap_create_exp_ord_comp_assess.tab

spool off
