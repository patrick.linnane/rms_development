/*********************************************************************************************************/
/*        Name: exp_gap_exp_trg_cost_comp_upd_aiudr.trg
/* Object Type: Trigger Create Script
/* Description: Creates a trigger exp_trg_cost_comp_upd_aiudr on RMS14.COST_COMP_UPD_STG
/*              to track the cost component updates.
/*
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 21-MAY-2013  Baranidharan  G           Initial Version
/*********************************************************************************************************/

set verify off
set trimspool on
set pages 0

PROMPT Creating Trigger exp_trg_cost_comp_upd_aiudr.trg

CREATE OR REPLACE TRIGGER EXP_TRG_COST_COMP_UPD_AIUDR
AFTER DELETE OR INSERT OR UPDATE OF SEQ_NO, COMP_ID, EFFECTIVE_DATE
   ON COST_COMP_UPD_STG
  FOR EACH ROW

DECLARE

   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE := NULL;
   L_comp_id                   COST_COMP_UPD_STG.COMP_ID%TYPE := :new.comp_id;
   L_exists                    NUMBER(5,0) := 0;

   CURSOR comp_exist IS
   SELECT count(*)
     FROM exp_cost_comp_temp
    WHERE comp_id = L_comp_id;

BEGIN

    IF DELETING THEN
        L_comp_id := :old.comp_id ;
    END IF;

    OPEN comp_exist;
        FETCH comp_exist INTO L_exists;
    CLOSE comp_exist;

    IF L_exists = 0 THEN
        INSERT INTO exp_cost_comp_temp (comp_id) VALUES (L_comp_id);
    END IF;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                             SQLERRM,
                                            'EXP_TRG_COST_COMP_UPD_AIUDR.TRG',
                                             TO_CHAR(SQLCODE));
      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END EXP_TRG_COST_COMP_UPD_AIUDR;
/

