/********************************************************************************************************/
/*        Name: exp_gap_exp_trg_ord_comp_assess_aur.trg
/* Object Type: Trigger Create Script
/* Description: Creates a trigger exp_trg_ord_comp_assess_aur on RMS14.ordsku_hts_assess
/*              to track the cost component updates.
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 29-MAY-2014  Baranidharan  G           Initial Version
/*********************************************************************************************************/

set verify off
set trimspool on
set pages 0

PROMPT Creating Trigger exp_trg_ord_comp_assess_aur.trg

CREATE OR REPLACE TRIGGER exp_trg_ord_comp_assess_aur
AFTER UPDATE OF EST_ASSESS_VALUE
   ON ORDSKU_HTS_ASSESS
  FOR EACH ROW

DECLARE

   L_error_message             RTK_ERRORS.RTK_TEXT%TYPE;
   L_username                  VARCHAR2(20);
   L_comp_id                   EXP_ORD_COMP_ASSESS.COMP_ID%TYPE := :new.comp_id;
   L_defaulted_from            EXP_ORD_COMP_ASSESS.DEFAULTED_FROM%TYPE := 'M';
   L_exists                    NUMBER(5,0) := 0;
   L_item                      ORDSKU_HTS.ITEM%TYPE;
   L_HTS                       ORDSKU_HTS.HTS%TYPE;
   L_IMP_CTRY                  ORDSKU_HTS.IMPORT_COUNTRY_ID%TYPE;
   L_ORIGIN_CTRY               ORDSKU_HTS.ORIGIN_COUNTRY_ID%TYPE;
   L_EFFECT_FROM               ORDSKU_HTS.EFFECT_FROM%TYPE;
   L_EFFECT_TO                 ORDSKU_HTS.EFFECT_TO%TYPE;
   L_ITEM_HTS_COMP_RATE        ITEM_HTS_ASSESS.COMP_RATE%TYPE;
   L_hts_exists                VARCHAR2(1) := 'Y';

   CURSOR C_exists
   IS
   SELECT count(*)
     FROM exp_ord_comp_assess
    WHERE comp_id  = :new.comp_id
      AND order_no = :new.order_no
      AND seq_no   = :new.seq_no;

   CURSOR C_ord_hts
   IS
   SELECT item,
          hts,
          import_country_id,
          origin_country_id,
          effect_from,
          effect_to
     FROM ordsku_hts
    WHERE order_no = :new.order_no
      AND seq_no   = :new.seq_no;

   CURSOR C_item_hts
   IS
   SELECT comp_rate
     FROM item_hts_assess
    WHERE item = l_item
      AND hts  = l_hts
      AND import_country_id = l_imp_ctry
      AND origin_country_id = l_origin_ctry
      AND effect_from = l_effect_from
      AND effect_to = l_effect_to
      AND comp_id = l_comp_id;

BEGIN

    SELECT user INTO L_username
      FROM dual;

    IF L_username <> 'RMS14' THEN

        OPEN C_ord_hts;
            FETCH C_ord_hts INTO L_ITEM,
                                 L_HTS,
                                 L_IMP_CTRY,
                                 L_ORIGIN_CTRY,
                                 L_EFFECT_FROM,
                                 L_EFFECT_TO;
        CLOSE C_ord_hts;

        OPEN C_item_hts;
            FETCH C_item_hts INTO L_ITEM_HTS_COMP_RATE;
        IF C_item_hts%NOTFOUND THEN
               L_hts_exists := 'N';
        END IF;
        CLOSE C_item_hts;

        IF ((( L_ITEM_HTS_COMP_RATE <> :new.comp_rate) AND L_hts_exists = 'Y') OR ( L_hts_exists = 'N')) THEN
            OPEN C_exists;
            FETCH C_exists INTO L_exists;
            CLOSE C_exists;

            IF ( L_exists = 0 ) THEN
                INSERT INTO exp_ord_comp_assess (comp_id,
                                                 order_no,
                                                 seq_no,
                                                 defaulted_from)
                                          VALUES(L_comp_id,
                                                 :new.order_no,
                                                 :new.seq_no,
                                                 L_defaulted_from);
            END IF;
        END IF;
    END IF;

EXCEPTION
   when OTHERS then
      L_error_message := SQL_LIB.CREATE_MSG('PACKAGE_ERROR',
                                            SQLERRM,
                                            'EXP_TRG_ORD_COMP_ASSESS_AUR.TRG',
                                            TO_CHAR(SQLCODE));
      RAISE_APPLICATION_ERROR(API_LIBRARY.TRIGGER_EXCEPTION, L_error_message);

END EXP_TRG_ORD_COMP_ASSESS_AUR;
/


