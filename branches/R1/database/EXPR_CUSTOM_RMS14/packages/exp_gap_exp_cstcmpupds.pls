/*********************************************************************************************************/
/*        Name: exp_gap_exp_cstcmpupds.pls
/* Object Type: Package Specification
/* Description: Express Cost Componet Update SQL package body 
/*
/* Modification History:
/* Date         Name                      Modification Description
/* -----------  ------------------------  ------------------------
/* 20-MAY-2014  Baranidharan  G           Initial Version
/*********************************************************************************************************/
-------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE
PACKAGE EXP_COST_COMP_UPD_SQL AS
---------------------------------------------------------------------------------------------
-- Function Name: PROCESS_ORD_COST_COMP_UPDATES
-- Purpose: The function is called by a batch/shell script (exp_batch_ordcostcompupd.ksh)
--          which is scheduled to run at the end of the day.
--          The function calls the functions to cascade the HTS, EXPENSES to the orders.
---------------------------------------------------------------------------------------------
FUNCTION PROCESS_ORD_COST_COMP_UPDATES (O_error_message          IN OUT  RTK_ERRORS.RTK_TEXT%TYPE,
                                        I_Thread_no              IN      RESTART_CONTROL.NUM_THREADS%TYPE,
                                        I_Threads                IN      RESTART_CONTROL.NUM_THREADS%TYPE)
RETURN BOOLEAN;

------------------------------------------------------------------------------------------------------
END;
/

